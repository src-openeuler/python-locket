%global _empty_manifest_terminate_build 0
Name:		python-locket
Version:	1.0.0
Release:	1
Summary:	File-based locks for Python for Linux and Windows
License:	BSD-2-Clause
URL:		http://github.com/mwilliamson/locket.py
Source0:        https://github.com/mwilliamson/locket.py/archive/%{version}.tar.gz#/locket-%{version}.tar.gz
BuildArch:	noarch


%description
Locket implements a lock that can be used by multiple processes provided they use the same path.

%package -n python3-locket
Summary:	File-based locks for Python for Linux and Windows
Provides:	python-locket
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-locket
Locket implements a lock that can be used by multiple processes provided they use the same path.

%package help
Summary:	Development documents and examples for locket
Provides:	python3-locket-doc
%description help
Locket implements a lock that can be used by multiple processes provided they use the same path.

%prep
%setup -q -n locket.py-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-locket -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 01 2022 liukuo <liukuo@kylinos.cn> - 1.0.0-1
- Update to 1.0.0

* Fri Jun 18 2021 Python_Bot <Python_Bot@openeuler.org> - 0.2.1-1
- Package Spec generated
